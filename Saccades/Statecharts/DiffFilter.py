# -*- coding: utf-8 -*-

from collections import deque
import numpy as np, sys
from constants import *

class DiffFilter(object):
    def __init__(self):
        self.x       = deque(maxlen=VELOCITY_LEN)
        self.y       = deque(maxlen=VELOCITY_LEN)
        self.t       = deque(maxlen=VELOCITY_LEN)
        self.kernel  = np.concatenate( (np.tile(-1.0, VELOCITY_LEN/2), np.array([0.0]), np.tile(1.0, VELOCITY_LEN/2)) )
        self.kernel /= (VELOCITY_LEN/2)*(VELOCITY_LEN/2+1)
        
    def addSample(self, x, y, t):
        self.x.append(x)
        self.y.append(y)
        self.t.append(t)
        if len(self.x) == VELOCITY_LEN:
            dx = np.correlate(self.x, self.kernel, "valid")
            dy = np.correlate(self.y, self.kernel, "valid")
            vel = 500*1.0/40 * np.sqrt(dx**2 + dy**2)            
            return vel[0]
        return -1

if __name__ == "__main__":
    print "Testing differential filter"
    X = np.arange(200)
    Y = np.concatenate( (np.tile(0, 85), np.arange(0, 1200 , 40), np.tile(1200, 85)) )
    T = np.arange(0, 400, 2)
    ###Y = [x * (1.0/(abs(100.5-x))) for x in X]
    import matplotlib.pyplot as plt
    plt.plot(X[VELOCITY_LEN/2:-VELOCITY_LEN/2], Y[VELOCITY_LEN/2:-VELOCITY_LEN/2], "r.-")
    V = []
    filter = DiffFilter()
    for x, y, t in zip(X, Y, T):
        v = filter.addSample(x, y, t)
        if v != -1:
            V.append(v)
            
    #print len(X[VELOCITY_LEN/2:-VELOCITY_LEN/2]), len(V)
    plt.plot(X[VELOCITY_LEN/2:-VELOCITY_LEN/2+1], V, "b.-")
    plt.show()
