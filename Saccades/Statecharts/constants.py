# -*- coding: utf-8 -*-

ONE_DEGREE          = 40 # Convert between degrees and pixels

# To set undefined, needs window size and percentage of invalids
UNDEF_WINDOW_LEN    = 20                        # Window size to see undefined
UNDEF_PERCENT       = 10                        # Percent above which undefined state is activated

# Thresholds to detect invalid samples, like (0,0) eye coordinates or pupil diameter too small
COORD_EPS           = 1e-2
PUPIL_EPS           = 10e-1

# Number of samples to LEARN noise level to detect saccades
NOISE_LEVEL_LEN     = 30  # About 60 ms


#######################################################################################
### Hitoshi
### CONSTANT DEFINITIONS

TRACKER_FREQ = 500   # Hz - eye tracker frequency
TRACKER_STEP = 2     # ms - eye tracker interval between samples 

SMOOTHING_LEN = 5    # tamanho da janela para suavização
VELOCITY_LEN  = 11   # 5 para esquerda e 5 para a direita

BUFFER_SIZE = 250    # number of recent samples to keep, changed from 2000 (about 4 seconds at 500 Hz) to 250 (about 500 ms at 500 Hz) 
MIN_FIX_DUR = 50     # ms mininum fixation duration
MIN_FIX_VAL = 90     # porcentagem das amostras da janela que devem ser válidas
MAX_FIX_VAR = 1.0    # maximum fixation variance

"""
MIN_SSAC_VEL = 3                 # vezes o nível de ruído - Small Saccade min velocity
MIN_SSAC_AMP = MAX_FIX_VAR       # Small Saccade min amplitude
MIN_SSAC_DUR = 20 # ms
"""

SACC_ONSET_COEF     = 1.7   # Coefficient to determine saccade onset = MEAN + SACC_ONSET_COEFF * STD
SACC_OFFSET_COEF    = 0.4   # Coefficient to determine saccade offset = THRESHOLD_ONSET * SACC_OFFSET_COEFF
MIN_SACC_ONSET_DUR  = 6     # Minimum duration of saccade onset to go to saccade
MIN_SACC_ONSET_DIST = 0.5 * ONE_DEGREE # Minimum distance of saccade onset to go to saccade
MAX_SACCADE_OFFSET_DURATION = 10 # Max saccade offset duration 


MIN_BSAC_AMP = 2 * MAX_FIX_VAR   # Big Saccade min amplitude
MIN_BSAC_DUR = 25 # ms


POS_SAC_FIX_DUR = 0.5 * MIN_FIX_DUR
MIN_BLINK_LEN = 100      # ms
MAX_BLINK_LEN = 1000     # ms

FRAME_DELAY = 3
## ideia: remover pequenos ruídos pela introdução de uma pequena latência.
## um frame delay de N permitiria corrigir/interpolar uma sequencia de N-1 undef samples.
## por exemplo, para N = 3
## 1 1 1 1 0 1
## 1 1 1 0 0 1
## essas sequencias com 0 (undef) poderiam ser facilmente interpoladas

# Estados do Olhar

# STATES
FIXATION    = 0
SACCADE     = 6
DRIFT       = 7
SHORT       = 1
LONG        = 2
NOISE       = 3
BLINK       = 4
LOST        = 5

### historico dos eventos para inferir comportamento, como frequencia de piscamento, leitura, etc
MAX_HISTORY_SIZE = 1000

### buffers de posicao e velocidade
UNDEFINED = 0
DEFINED   = 1

# Default velocity threshold used
## DEFAULT_VEL_TH = 50
MIN_VEL_TH     = 25
MAX_VEL_TH     = 65