class Eye(object):
    def __init__(self, x, y, p):
        self.x = x
        self.y = y
        self.pupil = p

class GazeSample(object):
    def __init__(self, t, xl, yl, pl, xr, yr, pr):
        self.left       = Eye(xl, yl, pl)
        self.right      = Eye(xr, yr, pr)
        self.tstamp     = t
        
class TimePoint(object):
    def __init__(self, t, x, y):
        self.t = t
        self.x = x
        self.y = y