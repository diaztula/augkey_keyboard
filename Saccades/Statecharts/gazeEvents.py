    # -*- coding: utf-8 -*-
"""
Eye tracking data processing

- Fixation
- Saccade
- Drift
- Undef
-- noise
-- blink
-- lost eye


Savitzky-Golay filter: coefs for a 5-point quadratic polynomial
Y[j] = (1.0/35) * (-3 Y[j-2] + 12 Y[j-1] + 17 Y[j] + 12 Y[j+1] -3 Y[j+2]
"""

# Antonio
import math, numpy as np, matplotlib.pyplot as plt
import LinearFilter, DiffFilter, InterpolateFilter, NormalDist, GazeSample, GazeDataReader
from constants import *
from collections import deque

import warnings
warnings.filterwarnings("error")

# ============================================================

class EyeGazeState:
    def __init__(self, st, ini=0, length=0):
        self.type = st
        self.ini = ini
        self.len = length
        self.end = 0
        self.x   = 0
        self.y   = 0
        self.isSaccOnset  = 1
        self.isSaccOffset = 1

    def set(self, st, ini=0, end=0, length=0, x=0, y=0, isSaccOnset=0, isSaccOffset=0):
        self.type  = st
        self.ini = ini
        self.end = end
        self.len = length
        self.x   = x
        self.y   = y
        self.isSaccOnset  = isSaccOnset
        self.isSaccOffset = isSaccOffset

    def setFromState(self, state):        
        self.type   = state.type
        self.ini    = state.ini
        self.len    = state.len
        self.end    = state.end
        self.x      = state.x
        self.y      = state.y 
        self.isSaccOnset  = state.isSaccOnset
        self.isSaccOffset = state.isSaccOffset

    def clone(self):
        new_st = EyeGazeState(self.type, self.ini, self.len)
        new_st.end = self.end
        new_st.x   = self.x
        new_st.y   = self.y
        new_st.isSaccOnset  = self.isSaccOnset
        new_st.isSaccOffset = self.isSaccOffset  
        return new_st
# ============================================================

class GazeEvents(object):
    def __init__(self, plot=False, count=0):
        self.linfilt        = LinearFilter.LinearFilter()
        self.diffilt        = DiffFilter.DiffFilter()
        self.interpol       = InterpolateFilter.InterpolateFilter()
        self.velNoise       = NormalDist.NormalDist(coef=SACC_ONSET_COEF)
        self.velThreshold   = -1
        # Filas baseadas em deque
        self.histQ       = deque(maxlen=MAX_HISTORY_SIZE) # CircularQueue(MAX_HISTORY_SIZE) # uns 1000 estados deve dar uns 10 segundos
        self.rawQ        = deque(maxlen=BUFFER_SIZE)      # CircularQueue(BUFFER_SIZE)
        self.smoothQ_X   = deque(maxlen=BUFFER_SIZE)      # CircularQueue(BUFFER_SIZE)
        self.smoothQ_Y   = deque(maxlen=BUFFER_SIZE)      # CircularQueue(BUFFER_SIZE)
        self.smoothQ_T   = deque(maxlen=BUFFER_SIZE)      # CircularQueue(BUFFER_SIZE)
        self.velocityQ   = deque(maxlen=BUFFER_SIZE)      # CircularQueue(BUFFER_SIZE)
        # ---------------------------------------
        self.isSaccade   = 0
        self.isSaccOffset= 0
        self.isSaccOnset = 0
        self.isFixation  = 0
        self.isDrift     = 0
        self.t0          = 0
        # ---------------------------------------
        self.frame_count = 0
        self.current_state = EyeGazeState(LOST, self.frame_count)
        self.last_state    = EyeGazeState(LOST, self.frame_count)
        self.noise_count = 0
        self.blink_count = 0
        # Testing interface with C/C++
        # Idea: if the object that represents the current state never changes (i.e. it is always the same object) can we use 
        # a single reference retrieved from C++ to get its parameter values?
        self.fakestate = EyeGazeState(LOST, self.frame_count)
        # ---------------------------------------
        self.plot = plot
        if plot:
            self.count = count
            self.f1   = plt.figure()
            self.p1   = self.f1.add_subplot(411)
            self.p1.set_xlim(0, count*TRACKER_STEP+10)
            self.p1.set_ylabel("Smoothed X (pixels)", fontsize=8)
            self.p2   = self.f1.add_subplot(412)
            self.p2.set_xlim(0, count*TRACKER_STEP+10)
            self.p2.set_ylabel("Gaze velocity (deg/s)", fontsize=8)
            self.p2.plot(-100, 0, "r.-", label="Saccade threshold")
            self.p2.plot(-100, 0, "b.-", label="Velocity")
            self.p2.legend(loc=2, fontsize=8)
            #self.f2   = self.plot.figure()
            self.p3   = self.f1.add_subplot(413)
            self.p3.set_xlim(0, count*TRACKER_STEP+10)
            self.p4   = self.f1.add_subplot(414)
            self.p4.set_ylim(-1, 8)
            self.p4.set_xlim(0, count*TRACKER_STEP+10)
            self.p4.set_ylabel("Gaze events", fontsize=8)
            self.p4.set_yticks(np.arange(-1, 9, 1))
            self.p4.set_yticklabels(["", "FIXATION", "", "", "NOISE", "BLINK", "LOST", "SACCADE", "DRIFT"], fontsize=8)
            
    def clearGazeEvents(self):
        """Clear all gaze events"""
        self.isSaccade = self.isFixation = self.isDrift = self.isSaccOffset = 0
                
    def addSampleList(self, t, xl, yl, pl, xr, yr, pr):
        sample = GazeSample.GazeSample(t, xl, yl, pl, xr, yr, pr)
        self.addSample(sample)

    def addSample(self, sample):
        """Adds a new sample, updates gaze state"""
        self.defined, self.x, self.y, self.t = self.interpol.addSample(sample)
        if self.defined: 
            # We have a defined sample!!!
            self.rawQ.append(GazeSample.TimePoint(self.t, self.x, self.y))    
            # Enqueue raw sample
            self.smoothX, self.smoothY = self.linfilt.addSample(self.x, self.y) 
            # Do we have smoothed sample???
            if self.smoothX != -1 and self.smoothY != -1:     
                # Yes, we have smoothed sample!!!
                self.smoothQ_X.append(self.smoothX) # Enqueue smoothed coordinates 
                self.smoothQ_Y.append(self.smoothY) 
                self.smoothQ_T.append(self.t) 
                if self.plot and self.frame_count < self.count:
                    self.p1.plot(self.t, self.smoothX, "r.", markersize=1.5)
                self.vel = self.diffilt.addSample(self.smoothX, self.smoothY, self.t) # Compute velocity
                # Do we have velocity?
                if self.vel != -1: # Yes, we have velocity!!!
                    #print "amostra valida: %i (%i, %i)   filtrada: (%i, %i), velocidade: %i"%(self.t, self.x, self.y, self.smoothX, self.smoothY, self.vel)
                    self.velocityQ.append(self.vel)
                    if self.plot and self.frame_count < self.count:
                        self.p2.plot(self.t, self.vel, "b.", markersize=1.5)
                else:
                    self.defined = 0
            else:
                self.defined = 0
        # Updates statechart
        # print self.defined, self.t
        self.updateState()
        #print self.current_state.type
        self.frame_count += 1   

    def updateState(self):
        """Updates the state chart according to the current state and the gaze, that can be FIXATION,SACCADE,DRIFT,LOST,NOISE, and BLINK"""
        # --------------------------- LOST -----------------------------------
        if self.current_state.type == LOST:
            if not self.defined:  ## UNDEFINED, continua em LOST
                self.current_state.len = self.t - self.current_state.ini
            else:               # De LOST vai para DRIFT
                self.current_state.end = self.t
                self.last_state.setFromState(self.current_state)
                self.current_state.set(st=DRIFT, ini=self.t, x=self.smoothX, y=self.smoothY)
            
        # --------------------------- BLINK ----------------------------------
        elif self.current_state.type == BLINK:
            if not self.defined:                # Posicao invalida, UNDEFINED
                self.current_state.len = self.t - self.current_state.ini # mais uma amostra pro blink
                # if self.current_state.len > MAX_BLINK_LEN:  ## blink becomes lost
                if self.current_state.len > MAX_BLINK_LEN:  ## blink becomes lost
                    self.current_state.set(st=LOST, ini=self.t, x = 0,  y = 0)
            else:
                self.current_state.end = self.t             # Acabou o blink
                ## WE CONTINUE THE PREV STATE BUT FOR SACCADES -- see state machine
                lastType   = self.last_state.type
                lastIni    = self.last_state.ini
                newLen     = self.last_state.len + self.current_state.len
                lastOnset  = self.last_state.isSaccOnset
                lastOffset = self.last_state.isSaccOffset
                self.last_state.setFromState(self.current_state)
                if lastType == SACCADE:    # Se o anterior foi sacada, vai para DRIFT
                    ### new_st = EyeGazeState(DRIFT, self.frame_count)
                    self.current_state.set(st=DRIFT, ini=self.t, x=self.smoothX, y=self.smoothY)
                else:                       # Se o anteroir no era sacada
                    self.current_state.set(st=lastType, ini=lastIni, length=newLen, x=self.smoothX, y=self.smoothY, isSaccOnset=lastOnset, isSaccOffset=lastOffset)
    
        # --------------------------- NOISE -----------------------------------
        elif self.current_state.type == NOISE: ## talvez noise nao precise entrar na historia, basta contar e saber a frequencia?
            if not self.defined:
                self.current_state.len = self.t - self.current_state.ini
                # if self.current_state.len > MIN_BLINK_LEN:
                if self.current_state.len > MIN_BLINK_LEN:
                    self.current_state.set(st=BLINK, ini=self.t, x = 0,  y = 0)
                    self.blink_count += 1
            else:
                self.current_state.end = self.t
                lastType   = self.last_state.type
                lastIni    = self.last_state.ini
                newLen     = self.last_state.len + self.current_state.len
                lastOnset  = self.last_state.isSaccOnset
                lastOffset = self.last_state.isSaccOffset
                self.last_state.setFromState(self.current_state)
                ## WE CONTINUE THE PREV STATE BUT FOR SACCADES -- see state machine, CHECK THIS PLEASE!!!!!!!!!!!!!!!!!!!!!!!!!!!
                ## Well, we continue in the last state even for saccades
                ### It seems reasonable not to include noise into the history, in part because it is easy to recover from blinks
                ### if the previous event is not noise (even when noise eventually leads to blink).
                ### This could allow for recording only the current and last events, to avoid heap overhead...
                ### By letting noise enter the history, we should check the type of event on top before resuming after a blink; 
                ### ### ### self.histQ.append(self.current_state.clone())  
                ### new_st = EyeGazeState(prev.type, prev.ini, prev.len + self.current_state.len)
                ### self.current_state = new_st
                self.current_state.set(st=lastType, ini=lastIni, length=newLen, x=self.smoothX, y=self.smoothY, isSaccOnset=lastOnset, isSaccOffset=lastOffset)
        else:
            # Current state is defined, so first of all we update current gaze event (fixation, saccade or drift?)
            # # # self.updateGazeEvent()
            if self.plot and self.frame_count < self.count:
                self.p2.plot(self.t, self.velThreshold, "r.", markersize=1.5)
                
            # --------------------------- DRIFT -----------------------------------
            if self.current_state.type == DRIFT:
                if not self.defined:
                    self.current_state.end = self.t
                    self.last_state.setFromState(self.current_state)
                    self.current_state.set(st=NOISE, ini=self.t, isSaccOnset=self.isSaccOnset, isSaccOffset=self.isSaccOffset)
                    self.isSaccOnset = 0
                else: # Defined, update statechart
                    # Saccade onset?
                    self.velThreshold = self.velNoise.getThreshold()
                    if self.vel >= self.velThreshold:
                        # NOTE: WE DO NOT ADD THESE SAMPLES TO THE NOISE QUEUE
                        if not self.isSaccOnset: # First time going to sacc onset
                            self.isSaccOnset = 1
                            self.saccOnsetStart = self.t
                            self.saccOnset_x    = self.smoothX
                            self.saccOnset_y    = self.smoothY
                            self.saccOnsetFrame = self.frame_count
                        else: # Already detecting saccade onset, check if goes to saccade
                            if (self.t - self.saccOnsetStart) >= MIN_SACC_ONSET_DUR and math.sqrt((self.smoothX - self.saccOnset_x)**2 + (self.smoothY - self.saccOnset_y)**2) >= MIN_SACC_ONSET_DIST:
                                # cria sacada
                                self.current_state.end = self.saccOnsetStart
                                self.current_state.len = self.current_state.end - self.current_state.ini
                                self.last_state.setFromState(self.current_state)
                                self.current_state.set(st=SACCADE, ini=self.saccOnsetStart, x=self.smoothX, y=self.smoothY)
                                self.isSaccOnset = 0 # limpando
                                self.isSaccOffset= 0
                                return # THIS RETURN HERE IS VERY IMPORTANT!!!!!!
                            else: # minimum duration/distance not fulfilled, stays...
                                pass 
                                # ADDITIONAL VALIDATION HERE MIGHT IMPROVE SACCADE ONSET DETECTION.....
                    else: # clean isSaccOnset
                        self.isSaccOnset = 0
                        
                    # Saccade onset is a tendency, so it does not change current state
                    # If not saccade onset then check for a fixation
                    if True or not self.isSaccOnset: # Not a saccade, then it may be a fixation or a drift
                        # First, add velocity to the velocity noise list
                        self.velNoise.addSample(self.vel)
                        n = int( math.ceil(MIN_FIX_DUR / (1000.0/TRACKER_FREQ)) ) # Number of samples to cover minimun fixation duration
                        # If last event was a saccade, then to compute dispersion we do not consider saccade points
                        # It means that after a saccade, the state will be drift until enough non-saccadic samples arrive and dispersion eventually is small
                        ### if len(self.smoothQ_X) >= n and (self.last_state.type != SACCADE or self.frame_count-n >= self.last_state.end):
                        if len(self.smoothQ_X) >= n and (self.last_state.type != SACCADE or self.t >= self.last_state.end + MIN_FIX_DUR):
                            l       = len(self.smoothQ_X)-1
                            idx     = l
                            initial = max(0, l-n)
                            while idx >= initial and (self.t - self.smoothQ_T[idx]) <= MIN_FIX_DUR:
                                idx -= 1
                            idx += 1
                            ### print "Diff t: ", self.t - self.smoothQ_T[idx]
                            lisX  = list(self.smoothQ_X)[idx:]
                            lisY  = list(self.smoothQ_Y)[idx:]
                            if len(lisX) > 1:
                                std  = (np.std(lisX, ddof=1) + np.std(lisY, ddof=1))/2
                                if self.plot:
                                    self.p3.plot(self.t, std, "b.", markersize=1.5)
                                if std <= MAX_FIX_VAR*ONE_DEGREE: # We have a fixation!
                                    fixX = np.median(lisX)
                                    fixY = np.median(lisY)
                                    self.current_state.end = self.t
                                    self.last_state.setFromState(self.current_state)
                                    # SHOULD WE KEEP isSaccOnset and/or isSaccOffset ??????????????????????????????????????????????????????????????????????????????????????????????
                                    self.current_state.set(st=FIXATION, ini=self.t, x=fixX, y=fixY)
                                else: # Stays a drift...
                                    self.current_state.len = self.t - self.current_state.ini
                                    self.current_state.x = self.smoothX
                                    self.current_state.y = self.smoothY                  
                            else: # yeap, stays a drift
                                self.current_state.len = self.t - self.current_state.ini
                                self.current_state.x = self.smoothX
                                self.current_state.y = self.smoothY                  
                        else: # still stays a drift...
                            self.current_state.len = self.t - self.current_state.ini
                            self.current_state.x = self.smoothX
                            self.current_state.y = self.smoothY
                    else: # SAccade onset, wait...
                        self.current_state.len = self.t - self.current_state.ini
                        self.current_state.x = self.smoothX
                        self.current_state.y = self.smoothY
                                                  
            # --------------------------- FIXATION -----------------------------------
            elif self.current_state.type == FIXATION:
                if not self.defined:
                    self.current_state.end = self.t
                    self.last_state.setFromState(self.current_state)
                    self.current_state.set(st=NOISE, ini=self.t, isSaccOnset=self.isSaccOnset, isSaccOffset=self.isSaccOffset)
                else: # State is defined, so lets implement the statechart
                    # Saccade onset?
                    self.velThreshold = self.velNoise.getThreshold()
                    if self.vel >= self.velThreshold:
                        # NOTE: WE DO NOT ADD THESE SAMPLES TO THE NOISE QUEUE
                        if not self.isSaccOnset: # First time going to sacc onset
                            self.isSaccOnset = 1
                            self.saccOnsetStart = self.t
                            self.saccOnset_x    = self.smoothX
                            self.saccOnset_y    = self.smoothY
                            self.saccOnsetFrame = self.frame_count
                        else: # Already detecting saccade onset, check if goes to saccad
                            if (self.t - self.saccOnsetStart) >= MIN_SACC_ONSET_DUR and math.sqrt((self.smoothX - self.saccOnset_x)**2 + (self.smoothY - self.saccOnset_y)**2) >= MIN_SACC_ONSET_DIST:
                                # cria sacada
                                self.current_state.end = self.saccOnsetStart
                                self.current_state.len = self.current_state.end - self.current_state.ini
                                self.last_state.setFromState(self.current_state)
                                self.current_state.set(st=SACCADE, ini=self.saccOnsetStart, x=self.smoothX, y=self.smoothY)
                                self.isSaccOnset = 0 # limpando
                                self.isSaccOffset= 0
                                return
                            else: # minimum duration/distance not fulfilled, stays...
                                pass 
                    else: # clean isSaccOnset
                        self.isSaccOnset = 0
                    # Saccade onset is a tendency, so it does not change current state
                    # IF not saccade onset, then check for a fixation
                    if True or not self.isSaccOnset:
                        # Not a saccade, then it may be a fixation or a drift
                        # First, add velocity to the velocity noise list
                        self.velNoise.addSample(self.vel)
                        n = int( math.ceil(MIN_FIX_DUR / (1000.0/TRACKER_FREQ)) ) # Number of samples to cover minimun fixation duration
                        # If last event was a saccade, then to compute dispersion we do not consider saccade points
                        # It means that after a saccade, the state will be drift until enough non-saccadic samples arrive and dispersion eventually is small
                        ### if len(self.smoothQ_X) >= n and (self.last_state.type != SACCADE or self.frame_count-n >= self.last_state.end):
                        if len(self.smoothQ_X) >= n and (self.last_state.type != SACCADE or self.t >= self.last_state.end + MIN_FIX_DUR):
                            l       = len(self.smoothQ_X)-1
                            idx     = l
                            initial = max(0, l-n)
                            while idx >= initial and (self.t - self.smoothQ_T[idx]) <= MIN_FIX_DUR:
                                idx -= 1
                            idx += 1
                            ### print "Diff t: ", self.t - self.smoothQ_T[idx]
                            lisX  = list(self.smoothQ_X)[idx:]
                            lisY  = list(self.smoothQ_Y)[idx:]
                            if len(lisX) > 1:
                                std  = (np.std(lisX, ddof=1) + np.std(lisY, ddof=1))/2
                                if self.plot:
                                    self.p3.plot(self.t, std, "b.", markersize=1.5)
                                if std <= MAX_FIX_VAR*ONE_DEGREE: # We stilll have a fixation!
                                    fixX = np.median(lisX)
                                    fixY = np.median(lisY)
                                    self.current_state.len = self.t - self.current_state.ini
                                    self.current_state.x = fixX
                                    self.current_state.y = fixY
                                else: # Go to drift
                                    self.current_state.end = self.t
                                    self.last_state.setFromState(self.current_state)
                                    # SHOULD WE KEEP isSaccOnset and/or isSaccOffset ??????????????????????????????????????????????????????????????????????????????????????????????
                                    self.current_state.set(st=DRIFT, ini=self.t, x=self.smoothX, y=self.smoothY)
                            else: # Go to drift
                                self.current_state.end = self.t
                                self.last_state.setFromState(self.current_state) 
                                # SHOULD WE KEEP isSaccOnset and/or isSaccOffset ??????????????????????????????????????????????????????????????????????????????????????????????
                                self.current_state.set(st=DRIFT, ini=self.t, x=self.smoothX, y=self.smoothY)
                        else: # Go to drift...
                            self.current_state.end = self.t
                            self.last_state.setFromState(self.current_state)
                            # SHOULD WE KEEP isSaccOnset and/or isSaccOffset ??????????????????????????????????????????????????????????????????????????????????????????????
                            self.current_state.set(st=DRIFT, ini=self.t, x=self.smoothX, y=self.smoothY)
                    else:
                        self.current_state.len = self.t - self.current_state.ini
                        self.current_state.x = self.smoothX
                        self.current_state.y = self.smoothY

            # --------------------------- SACCADE -----------------------------------
            elif self.current_state.type == SACCADE:
                if not self.defined:
                    self.current_state.end = self.t
                    self.last_state.setFromState(self.current_state)
                    self.current_state.set(st=NOISE, ini=self.t, isSaccOnset=0, isSaccOffset=self.isSaccOffset)
                else: # State is defined, so lets implement the statechart
                    self.velThreshold = self.velNoise.getThreshold()
                    if self.vel >= self.velThreshold:
                        self.current_state.len = self.t - self.current_state.ini
                        self.current_state.x = self.smoothX
                        self.current_state.y = self.smoothY
                    elif self.vel >= SACC_OFFSET_COEF*self.velThreshold and (not self.isSaccOffset or \
                        (self.t-self.saccOffsetStart < MAX_SACCADE_OFFSET_DURATION and self.vel < self.velocityQ[-2])): # Detecting saccade offset
                        self.current_state.len = self.t - self.current_state.ini
                        self.current_state.x = self.smoothX
                        self.current_state.y = self.smoothY
                        if not self.isSaccOffset:   # Transition from saccade to saccade offset
                            self.isSaccOffset = 1
                            self.saccOffsetStart = self.t
                        if self.plot:
                            self.p2.plot(self.t, self.vel, "g.", markersize=1.5)
                    else:
                        self.current_state.end = self.t
                        self.last_state.setFromState(self.current_state)
                        self.current_state.set(st=DRIFT, ini=self.t, x=self.smoothX, y=self.smoothY)
    
    
        """        else:
            print("E agora???? ")
            return None
        """
       #  print "Estado atual: ", self.current_state.type, "  duracao: ", self.current_state.len
        if self.plot and self.frame_count < self.count:
            self.p4.plot(self.t, self.current_state.type, "k.", markersize=1.5)
          
    def ping(self, x):
        print "response to ping: mean=%i"%(np.mean(np.array([x])))
  
    def fakeStateChange(self):
        self.fakestate.type = (self.fakestate.type + 1) % 8
        new_st = EyeGazeState(self.fakestate.type, self.frame_count)
        self.fakestate = new_st 
        #print self.fakestate.type
  
#============================================================
    
if __name__ == "__main__":    
    lastFrame   = 400
    count       = 0 

    # Antonio, detectar eventos do olhar
    gazeEvents = GazeEvents(plot=True, count=lastFrame)
    
    # carregar e processar os dados do olhar
    raw_data =  GazeDataReader.GazeDataReader() # list of gaze samples
    raw_data.openFile("../SwitchTest/data/fastSaccades/eye.csv")
    
    sample = raw_data.getNextSample()
    while sample!= -1 and count < lastFrame:
        gazeEvents.addSample(sample)  #
        count += 1
        sample = raw_data.getNextSample()
    
    if gazeEvents.plot:
        plt.savefig("fig_2states.png", dpi=1200)
        plt.show()

