# -*- coding: utf-8 -*-

from collections import deque
import numpy as np, sys
from GazeSample import *
from constants import *
from __builtin__ import True

class UndefinedFilter(object):
    def __init__(self):
        self.samples        = deque(maxlen=UNDEF_WINDOW_LEN)
        self.valid          = deque(maxlen=UNDEF_WINDOW_LEN)
        self.validCount     = 0
        self.defined = False
    
    def validSample(self, sample):
        if  (abs(sample.left.x ) <= COORD_EPS and abs(sample.left.y ) <= COORD_EPS) or\
            (abs(sample.right.x) <= COORD_EPS and abs(sample.right.y) <= COORD_EPS) or\
            (abs(sample.left.pupil) <= PUPIL_EPS) or (abs(sample.right.pupil) <= PUPIL_EPS):
            return 0
        return 1

    def addSample(self, sample):
        self.samples.append(sample)
        self.valid.append(self.validSample(sample))
        self.validCount = np.sum(self.valid)
        ###print len(self.samples), self.validCount, float(self.validCount) / float(len(self.samples))*100
        if float(self.validCount) / float(len(self.samples))*100 <= UNDEF_PERCENT:
            self.defined = False
        else:
            self.defined = True
        return self.defined
              
    def isDefined(self):
        return self.defined        
       
    def interpolate(self):
        if self.defined:
            count = 0
            idx   = len(self.valid)
            
            while idx >= 0 and count <= FRAME_DELAY-1:
                
                 
        
        pass
                
    def addSample111(self, sample):
        self.samples.append(sample)
        self.validCount = 0
        l=[]
        for s in list(self.samples):
            if self.isValid(s):
                self.validCount += 1
        ###print len(self.samples), self.validCount, float(self.validCount) / float(len(self.samples))*100
        if float(self.validCount) / float(len(self.samples))*100 < UNDEF_PERCENT:
            return False
        return True
                
if __name__ == "__main__":
    print "Testing valid filter"
    import csv
    if len(sys.argv) == 2:
        f = file(sys.argv[1])
    else:
        f = file("../SwitchTest/data/fastSaccades/eye.csv")
    reader = csv.reader(f)
    filter = UndefinedFilter()
    count = 0
    X = []
    Y = []
    Z = []
    for d in reader:
        tstamp, leftx, lefty = int(float(d[0]))/10, int(float(d[1])), int(float(d[2]))
        leftpupil = float(d[3])
        rightx, righty = int(float(d[4])), int(float(d[5]))
        rightpupil = float(d[6])
        s = GazeSample(tstamp, leftx, lefty, leftpupil, rightx, righty, rightpupil)
        X.append(count)
        if filter.undefined(s):
            Y.append(0)
        else:
            Y.append(1)
        if filter.isValid(s):
            Z.append(5)
        else:
            Z.append(6)            
        count += 1
    import matplotlib.pyplot as plt
    plt.ylim(0, 10)
    plt.plot(X, Y, "r.-")
    plt.plot(X, Z, "b.-")
    plt.show()
    
    