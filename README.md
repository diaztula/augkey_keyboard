# README #

This is an executable of the *AugKey* virtual keyboard. Here you can see a [video](https://www.youtube.com/watch?v=x2c0LXnCn-8) of AugKey we made for CHI'16. The original paper can be found here: [AugKey: Increasing Foveal Throughput in Eye Typing with Augmented Keys](http://dl.acm.org/citation.cfm?id=2858036.2858517).

The original implementation was targeted for Linux. The implementation provided here was ported to Windows 8 and is similar to the original keyboard, except for user data logging (that was needed during the eye typing experiments described in the paper) and it uses only the mouse position as pointing. The experiments described in the CHI'16 paper were performed with a SMI RED500 eye tracker. 

To run the keyboard, simply download all files and run the "augkey-original.exe" to see a demo of AugKey. The keyboard is pre-trained to offer word prediction in English. 

For word prediction we use [**presage**](http://presage.sourceforge.net/), a very nice word prediction software. The keyboard was implemented in C++ with SDL1.2 (your are right, it is an old library). 

**This implementation is a demo only and is not intended for commercial puporses.**

## Configuration file ##

If you want to test the keyboard with different size, edit the parameters *screen_width*, *screen_height*, *keyboard_width*, and *keyboard_height* in the file **Keyboard.cfg**. The first 2 parameters define the size of the window (not the screen!), and the last 2 parameters define the size of the keyboard (i.e. the keys with the letters, without considering the list of words). The difference between the screen and keyboard heights will be assigned to the text area height.

If you have any comments, doubts, or if you encounter a problem, please contact [Antonio Diaz Tula](mailto:diaztula@ime.usp.br)

Enjoy it!